EXE=go-template

GOJUNITREPORT=~/go/bin/go-junit-report

# Variable for filename for store running procees id
PID_FILE = /tmp/my-app.pid

# We can use such syntax to get main.go and other root Go files.
ALLGO_FILES = $(wildcard *.go)
ALLGOTEST_FILES = $(wildcard *_test.go)
GO_FILES = $(filter-out $(ALLGOTEST_FILES),$(ALLGO_FILES))

start: build
	./$(EXE) & echo $$! > $(PID_FILE)

stop:
	@echo "STOPPED $(EXE)" && printf '%*s\n' "40" '' | tr ' ' -
	-kill `cat $(PID_FILE)`
  
restart: stop start
	@echo "STARTED $(EXE)" && printf '%*s\n' "40" '' | tr ' ' -

serve: start
	fswatch -or --event=Updated . | \
	xargs -n1 -I {} make restart
  
prereq:

test: prereq
	go test . 2>&1 

test-integration: prereq
	go test . 2>&1 

build:
	go build .

cover:
	go test -coverprofile=c.out
	go tool cover -html=c.out

.PHONY: start stop restart serve prereq test test-integration build cover
