I wrote this project as a template for starting new go projects.  I'm trying to put into it boilerplate for a typical go application.  Currently this includes:

- viper for configuration
- zerolog for logging
- prometheus for metrics
- k8s for simple deployment
- Dockerfile for creating a container
- gitlib for CI/CD
- Makefile for controlling it all
- Ginko for testing
- optional http context
- signal handlers for shutting the application down

The application is organized in the following way:

- main.go - simple entrypoint to application.  This create and sets up an application context and runs the application
- context.go - context represents everything needed to run the application.  
- setup.go - sets up the context 
- main_test.go - Base for tests, everything should be injectable using the app context.  The base tests simply starts the app and issues a cancel to it.
