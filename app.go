package main

import "time"

// App This is the application that will run
func App(ctx *AppContext) error {

	// TODO - your app goes here
	for true {
		select {
		case <-ctx.ctx.Done():
			return ctx.ctx.Err()
		case <-time.After(5 * time.Second):
			ctx.log.Info().Msg("Tick . . .")
		}
	}

	return nil
}
