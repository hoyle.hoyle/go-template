package main

import (
	"context"
	"fmt"
	"github.com/rs/zerolog"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/spf13/viper"
)

// AppContext context for application
// TODO - add more to the context as needed, make sure you update NewAppContext and Setup
type AppContext struct {
	ctx         context.Context
	appName     string
	version     string
	environment string
	viper       *viper.Viper
	http        *http.Server
	log         zerolog.Logger
}

// WithContext update the context in the App Context
func (a *AppContext) WithContext(c context.Context) *AppContext {
	a.ctx = c
	return a
}

// NewAppContext Create a new app context
func NewAppContext(appName string) *AppContext {
	log := zerolog.New(os.Stderr).With().Timestamp().Logger()

	return &AppContext{
		context.Background(),
		appName,
		"",
		"",
		viper.New(),
		nil,
		log,
	}
}

// Info string about appliation
func (a *AppContext) Info() string {
	if a.environment != "production" {
		return fmt.Sprintf("%s v%s (%s)", a.appName, a.version, a.environment)
	}

	return fmt.Sprintf("%s v%s", a.appName, a.version)
}

func (a *AppContext) run(app func(*AppContext) error) {

	interruptChan := make(chan os.Signal, 1)
	signal.Notify(interruptChan, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	done := make(chan bool, 1)
	go func() {
		if err := app(a); err != nil {
			a.log.Fatal().Err(err)
		}
		done <- true
	}()

	// Wait for the app to be done or for another signal
	select {
	case <-done:
		a.log.Info().Msg("The application has completed.")
	case <-interruptChan:
		a.log.Info().Msg("The application has been interrupted")
	}

	if a.http != nil {
		// Create a deadline to wait for.
		cctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
		defer cancel()
		a.http.Shutdown(cctx)
	}

	a.log.Info().Msg("Shutting down")
	os.Exit(0)
}
