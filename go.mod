module gitlab.com/hoyle.hoyle/go-template

go 1.13

require (
	github.com/onsi/ginkgo v1.11.0
	github.com/onsi/gomega v1.8.1
	github.com/prometheus/client_golang v0.9.3
	github.com/rs/zerolog v1.17.2
	github.com/spf13/viper v1.6.2
	golang.org/x/sys v0.0.0-20200124204421-9fbb57f87de9 // indirect
)
