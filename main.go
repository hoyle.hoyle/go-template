package main

func main() {

	ctx := NewAppContext("gotemplate")

	if err := Setup(ctx); err != nil {
		panic(err)
	}
	ctx.log.Info().Msg(ctx.Info())

	// TODO - Add other listeners to ctx.http if this is a web app

	if ctx.http != nil {
		ctx.log.Info().Msg("Starting HTTP listener")
		go ctx.http.ListenAndServe()
	}

	ctx.run(App)
}
