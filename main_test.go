package main_test

import (
	"context"
	"fmt"
	"testing"
	"time"

	. "github.com/onsi/ginkgo"
	"github.com/onsi/ginkgo/reporters"
	. "github.com/onsi/gomega"

	main "gitlab.com/hoyle.hoyle/go-template"
)

func TestMain(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("junit.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "Main Suite", []Reporter{junitReporter})
}

var _ = Describe("Main", func() {
	var ctx *main.AppContext

	BeforeEach(func() {
		Context("Create AppContext", func() {
			ctx = main.NewAppContext("test")
			Expect(ctx).ShouldNot(BeNil())
		})
	})

	When("application is canceled", func() {
		It("should return a canceled context error", func() {
			cc, cancel := context.WithCancel(context.TODO())
			ctx = ctx.WithContext(cc)

			go func() {
				time.Sleep(time.Second * 1)
				cancel()
			}()

			err := main.App(ctx)

			Expect(err).Should(BeEquivalentTo(fmt.Errorf("context canceled")))
		})
	})
})
