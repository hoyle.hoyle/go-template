package main

import (
	"fmt"
	"net/http"
	"os"
	"time"

	"github.com/rs/zerolog"

	"github.com/prometheus/client_golang/prometheus/promhttp"
)

// Setup Setup the application
func Setup(ctx *AppContext) error {
	ctx.viper.SetConfigName("config")                              // name of config file (without extension)
	ctx.viper.SetConfigType("yaml")                                // REQUIRED if the config file does not have the extension in the name
	ctx.viper.AddConfigPath(fmt.Sprintf("/etc/%s/", ctx.appName))  // path to look for the config file in
	ctx.viper.AddConfigPath(fmt.Sprintf("$HOME/.%s", ctx.appName)) // call multiple times to add many search paths
	ctx.viper.AddConfigPath(".")                                   // optionally look for config in the working directory

	err := ctx.viper.ReadInConfig() // Find and read the config file
	if err != nil {                 // Handle errors reading the config file
		return fmt.Errorf("Fatal error config file: %s", err)
	}

	ctx.viper.SetDefault("listen-address", ":8080")
	ctx.viper.SetDefault("read-timeout", 10*time.Second)
	ctx.viper.SetDefault("write-timeout", 10*time.Second)
	ctx.viper.SetDefault("max-header-bytes", 1<<20)
	ctx.viper.SetDefault("prometheus-path", "/metrics")
	ctx.viper.SetDefault("environment", "develop")
	ctx.viper.SetDefault("version", "0.0.0")

	ctx.version = ctx.viper.GetString("version")
	ctx.environment = ctx.viper.GetString("environment")

	addr := ctx.viper.GetString("listen-address")
	readTimeout := ctx.viper.GetDuration("read-timeout")
	writeTimeout := ctx.viper.GetDuration("write-timeout")
	maxHeaderBytes := ctx.viper.GetInt("max-header-bytes")
	promPath := ctx.viper.GetString("prometheus-path")

	mux := http.NewServeMux()
	ctx.http = &http.Server{
		Addr:           addr,
		Handler:        mux,
		ReadTimeout:    readTimeout,
		WriteTimeout:   writeTimeout,
		MaxHeaderBytes: maxHeaderBytes,
	}
	mux.Handle(promPath, promhttp.Handler())

	if ctx.environment == "develop" {
		output := zerolog.ConsoleWriter{Out: os.Stderr}
		ctx.log = zerolog.New(output).With().Timestamp().Logger()
	}

	// TODO - Add more setup as needed

	return nil
}

func teardown(ctx *AppContext) {
	ctx.http.Shutdown(ctx.ctx)
}
